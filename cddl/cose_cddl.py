#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Copyright 2022 ARM Limited or its affiliates
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------------

import xml.etree.ElementTree as ET
import requests

cose_source = 'https://www.ietf.org/archive/id/draft-ietf-cose-msg-24.xml'
cose_filename = 'cose.xml'
cose_cddl_filename = 'cose.cddl'

with open(cose_filename, 'w') as fd:
    r = requests.get(cose_source)
    r.raise_for_status()
    fd.write(r.text)

# Load cose xml
xml_cose = ET.parse(cose_filename)
cddl_elements = xml_cose.findall("//artwork[@type='CDDL']")

cddl = '\n'.join([e.text for e in cddl_elements])

with open(cose_cddl_filename, 'w') as fd:
    fd.write(cddl)
