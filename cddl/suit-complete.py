#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Copyright 2022 ARM Limited or its affiliates
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------------

import cose_cddl
import suit_cddl

suit_complete_cddl = 'suit-complete.cddl'

with open(suit_complete_cddl, 'w') as sc_fd:
    with open(suit_cddl.suit_cddl_filename) as s_fd:
        sc_fd.write(s_fd.read())
    with open(cose_cddl.cose_cddl_filename) as c_fd:
        sc_fd.write(c_fd.read())
