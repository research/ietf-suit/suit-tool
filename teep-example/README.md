# TEEP Example use cases

To build the TEEP example, run:

```
../bin/suit-tool create -i personalisation.json -o personalisation.suit
```

The result should be approximately as follows:

personalisation.suit:

```
107({
        / authentication-wrapper / 2:<<[
            digest: <<[
                / algorithm-id / -16 / "sha256" /,
                / digest-bytes /
h'a7fd6593eac32eb4be578278e6540c5c09cfd7d4d234973054833b2b93030609'
            ]>>
            ]
        ]>>,
        / manifest / 3:<<{
            / manifest-version / 1:1,
            / manifest-sequence-number / 2:3,
            / common / 3:<<{
                / dependencies / 1:[
                    {
                        / dependency-digest / 1:[
                            / algorithm-id / -16 / "sha256" /,
                            / digest-bytes /
h'd6c1fc7200483092e2db59d4907f9b1505cb3af2795cf78f7ae3d88166fdf743'
                        ],
                    }
                ],
                / components / 2:[
                    [h'4f502d544545', h'44f301',
h'636f6e6669672e6a736f6e']
                ],
                / common-sequence / 4:<<[
                    / directive-set-component-index / 12,0 ,
                    / directive-override-parameters / 20,{
                        / vendor-id /
1:h'ec41787224345ae580003de697ff8d43' /
ec417872-2434-5ae5-8000-3de697ff8d43 /,
                        / class-id /
2:h'eb1701b48be85709aca0adf89f056a64' /
eb1701b4-8be8-5709-aca0-adf89f056a64 /,
                        / image-digest / 3:<<[
                            / algorithm-id / -16 / "sha256" /,
                            / digest-bytes /
h'aaabcccdeeef00012223444566678889abbbcdddefff01112333455567778999'
                        ]>>,
                    } ,
                    / condition-vendor-identifier / 1,15 ,
                    / condition-class-identifier / 2,15
                ]>>,
            }>>,
            / dependency-resolution / 7:<<[
                / directive-set-dependency-index / 13,0 ,
                / directive-set-parameters / 19,{
                    / uri /
21:'tam.teep.example/edd94cd8-9d9c-4cc8-9216-b3ad5a2d5b8a.suit',
                } ,
                / directive-fetch / 21,2 ,
                / condition-image-match / 3,15
            ]>>,
            / install / 9:<<[
                / directive-set-component-index / 12,0 ,
                / directive-set-parameters / 19,{
                    / uri / 21:'http://tam.teep.example/config.json',
                } ,
                / directive-set-dependency-index / 13,0 ,
                / directive-process-dependency / 18,0 ,
                / directive-set-component-index / 12,0 ,
                / directive-fetch / 21,2 ,
                / condition-image-match / 3,15
            ]>>,
            / validate / 10:<<[
                / directive-set-component-index / 12,0 ,
                / condition-image-match / 3,15 ,
                / directive-set-dependency-index / 13,0 ,
                / directive-process-dependency / 18,0
            ]>>,
            / run / 12:<<[
                / directive-set-dependency-index / 13,0 ,
                / directive-process-dependency / 18,0
            ]>>,
            / text / 13:<<{
                [h'4f502d544545', h'44f301',
h'636f6e6669672e6a736f6e']:{
                        / model-name / 2:'Personalised OP-TEE on TF-A
on TrustZone',
                        / vendor-domain / 3:'tam.teep.example',
                    }
                [h'4f502d544545', h'44f301',
h'edd94cd89d9c4cc89216b3ad5a2d5b8a', h'7461']:{
                        / model-name / 2:'OP-TEE on TF-A on
TrustZone',
                        / vendor-domain / 3:'teep.example',
                    }
            }>>,
        }>>,
    })
```

ta.suit:

```
107({
        / authentication-wrapper / 2:<<[
            digest: <<[
                / algorithm-id / -16 / "sha256" /,
                / digest-bytes /
h'd6c1fc7200483092e2db59d4907f9b1505cb3af2795cf78f7ae3d88166fdf743'
            ]>>,
            signature: <<18([
                    / protected / <<{
                        / alg / 1:-7 / "ES256" /,
                    }>>,
                    / unprotected / {
                    },
                    / payload / F6 / nil /,
                    / signature / h'0865e51aea00648bb3342d9c5b1a0b5687
a81e48bbd58e9acf65401a0c2db9dde1a1f041ef65ad3df9a0f68802dcee5be5c79680
95a5516789b4a312b13de49a'
                ])>>
            ]
        ]>>,
        / manifest / 3:<<{
            / manifest-version / 1:1,
            / manifest-sequence-number / 2:5,
            / common / 3:<<{
                / components / 2:[
                    [h'4f502d544545', h'44f301',
h'edd94cd89d9c4cc89216b3ad5a2d5b8a', h'7461']
                ],
                / common-sequence / 4:<<[
                    / directive-override-parameters / 20,{
                        / vendor-id /
1:h'c0ddd5f15243566087db4f5b0aa26c2f' /
c0ddd5f1-5243-5660-87db-4f5b0aa26c2f /,
                        / class-id /
2:h'db42f7093d8c55baa8c5265fc5820f4e' /
db42f709-3d8c-55ba-a8c5-265fc5820f4e /,
                        / image-digest / 3:<<[
                            / algorithm-id / -16 / "sha256" /,
                            / digest-bytes /
h'00112233445566778899aabbccddeeff0123456789abcdeffedcba9876543210'
                        ]>>,
                        / image-size / 14:76778,
                    } ,
                    / condition-vendor-identifier / 1,15 ,
                    / condition-class-identifier / 2,15
                ]>>,
            }>>,
            / install / 9:<<[
                / directive-set-parameters / 19,{
                    / uri /
21:'https://teep.example/edd94cd8-9d9c-4cc8-9216-b3ad5a2d5b8a.ta',
                } ,
                / directive-fetch / 21,2 ,
                / condition-image-match / 3,15
            ]>>,
            / validate / 10:<<[
                / condition-image-match / 3,15
            ]>>,
            / run / 12:<<[
                / directive-run / 23,2
            ]>>,
            / text / 13:<<{
                [h'4f502d544545', h'44f301',
h'edd94cd89d9c4cc89216b3ad5a2d5b8a', h'7461']:{
                        / model-name / 2:'OP-TEE on TF-A on
TrustZone',
                        / vendor-domain / 3:'teep.example',
                    }
            }>>,
        }>>,
    })
```