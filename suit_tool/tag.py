#!/usr/bin/python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Copyright 2022 ARM Limited or its affiliates
#
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------------
import cbor2 as cbor
from suit_tool.manifest import SUITEnvelopeTagged
envelope_tag_value = SUITEnvelopeTagged.fields['suit_envelope'].suit_key

def rm(options, envelope):
    if isinstance(envelope,cbor.CBORTag) and envelope.tag == envelope_tag_value:
        return envelope.value
    return envelope


def add(options, envelope):
    if isinstance(envelope,cbor.CBORTag) and envelope.tag == envelope_tag_value:
        return envelope
    return cbor.CBORTag(envelope_tag_value, envelope)

def main(options):
    # Read the manifest wrapper
    envelope = cbor.loads(options.manifest.read())
    if not hasattr(options, 'tag_action'):
        return 1

    new_envelope = {
        'rm' : rm,
        'add' : add
    }.get(options.tag_action)(options, envelope)

    if not new_envelope:
        return 1

    options.output_file.write(cbor.dumps(new_envelope, canonical=True))

    return 0
